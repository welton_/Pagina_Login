function validar() {
    var email = formuser.email.value;
    var senha = formuser.senha.value;


    if (email == "" || email.indexOf('@') == -1) {
        alert('Preencha o campo E-mail.');
        formuser.email.focus();
        return false;
    }


    if (email == "" || email.indexOf('.') == -1) {
        alert('Preencha o campo E-mail.');
        formuser.email.focus();
        return false;
    }


    if (email == "" || email.indexOf('_') == -1) {
        alert('Preencha o campo E-mail.');
        formuser.email.focus();
        return false;
    }

    if (email == "" || email.indexOf('com') == -1) {
        alert('Preencha o campo E-mail.');
        formuser.email.focus();
        return false;
    }

    if (email == "" || email.indexOf('br') == -1) {
        alert('Preencha o campo E-mail.');
        formuser.email.focus();
        return false;
    }


    if (senha == "" || senha.length > 6) {
        alert('Preencha o campo senha com no maximo 6 caracteres.');
        formuser.senha.focus();
        return false;
    }


    document.getElementById("log").innerHTML = "Você esta logado, Parabéns!";
}